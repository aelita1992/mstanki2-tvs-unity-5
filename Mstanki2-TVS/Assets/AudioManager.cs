﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AudioManager : MonoBehaviour {

	public static AudioClip smusic;
	public AudioClip music;
	public static float smusicVolume;
	public static float ssoundVolume;
	public static float soverallVolume;

	public float musicVolume;
	public float soundVolume;
	public float overallVolume;

	void Start () {
		smusic = this.music;
		smusicVolume = this.musicVolume;
		ssoundVolume = this.soundVolume;
		soverallVolume = this.overallVolume;
		PlayMusic (music);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public static void Play(AudioClip s)
	{
		GameObject go = new GameObject ("Audio: " +  s.name);
		AudioSource source = go.AddComponent<AudioSource>();
		source.clip = s;
		source.volume = ssoundVolume * soverallVolume;
		source.Play ();
		Destroy (go, s.length);
	}

	public static void PlayMusic(AudioClip s)
	{
		GameObject go = new GameObject ("Music: " +  s.name);
		AudioSource source = go.AddComponent<AudioSource>();
		source.clip = s;
		source.volume = smusicVolume * soverallVolume;
		source.loop = true;
		source.Play ();
		//Destroy (go, s.length);
	}

}
