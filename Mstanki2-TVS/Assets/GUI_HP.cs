﻿using UnityEngine;
using System.Collections;

public class GUI_HP : MonoBehaviour {

	public PlayerController player;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		this.GetComponent<GUIText>().text = "HP: " + PlayerController.HP + "  SH: " + PlayerController.shield + "  EN: " + PlayerController.energy;
		if(PlayerController.isDead == true)
			this.GetComponent<GUIText>().text = "HP: " + PlayerController.HP + "  SH: " + PlayerController.shield + "  EN: " + PlayerController.energy + "  DEAD";

	}
}
