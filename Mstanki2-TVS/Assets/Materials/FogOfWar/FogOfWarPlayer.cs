﻿using UnityEngine;
using System.Collections;

public class FogOfWarPlayer : MonoBehaviour {
	
	public Transform FogOfWarPlane;
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 screenPos = Camera.main.WorldToScreenPoint (transform.position);
		Ray rayToPlayerPos = Camera.main.ScreenPointToRay (screenPos);
		
		RaycastHit hit;
		if (Physics.Raycast (rayToPlayerPos, out hit, 1000)) {
			FogOfWarPlane.GetComponent<Renderer>().material.SetVector("_Player_Pos", hit.point);
		}

	
	}
//	public static Ray2D ScreenPointToRay2D(this Camera extends, Vector3 position)
//		{
//			Ray ray = extends.ScreenPointToRay(position);
//			return new Ray2D(ray.origin, ray.direction);
//		}

}
