﻿using UnityEngine;
using System.Collections;

public class AIMachine : AI {

	void Update()
	{
		switch(state)
		{

			case State.IDLE:
				if(path != null && pathTime + rnd < Time.time)
				{
					calcPath(lastPos);
					pathTime = Time.time;
					rnd = Random.Range (0.7f, 1.3f);
				}
				if (path != null && currentWaypoint < path.vectorPath.Count)
				{
					dir = (path.vectorPath[currentWaypoint]-transform.position).normalized;
					if (Vector3.Distance (transform.position, path.vectorPath[currentWaypoint]) < nextWaypointDistance)
						currentWaypoint++;
				}
				else 		
				if (path != null && currentWaypoint >= path.vectorPath.Count) { //osiagniety koniec sciezki
					lastPos = originalPos;
				}
				else 
					dir = new Vector3();
				
				enemy.move (dir, 0.5f);


				if(checkVisibility()==true) state = State.MOVE_DIR;
				break;

			case State.MOVE_DIR:
				dir = (enemy.player.transform.position - transform.position).normalized;
				enemy.move (dir, 1.0f);

				if(checkVisibility ()==false) state = State.MOVE_PATH;
				if(checkAttack () == true) state = State.ATTACK;
				break;

			case State.MOVE_PATH:

				if(path != null && pathTime + rnd < Time.time)
				{
					calcPath(lastPos);
					pathTime = Time.time;
					rnd = Random.Range (0.7f, 1.3f);
				}
				if (path != null && currentWaypoint < path.vectorPath.Count)
				{
					dir = (path.vectorPath[currentWaypoint]-transform.position).normalized;
					if (Vector3.Distance (transform.position, path.vectorPath[currentWaypoint]) < nextWaypointDistance)
						currentWaypoint++;
				}
				else 		
				if (path != null && currentWaypoint >= path.vectorPath.Count) { //osiagniety koniec sciezki
					lastPos = originalPos;
				}
				else 
					dir = new Vector3();
				
				enemy.move (dir, 1f);

				if(checkVisibility()==true) state = State.MOVE_DIR;
				break;

			case State.ATTACK:
				enemy.primAttack();
				orgSpeed = enemy.speed;
				//enemy.speed=0.0f;
				if(checkAttack ()==false) {
					state = State.MOVE_DIR;
					//enemy.speed = orgSpeed;
				}
				break;

		}
	}
}
