﻿using UnityEngine;
using System.Collections;

public class Enemy6AI : AI {

	void Update () {
		checkVisibility();
		
		if (isVisible==true) checkAttack ();
		
		if(isVisible==true && canAttack==false)
		{
			if(pathTime+0.5f < Time.time)
			{
				calcPath(lastPos);
				pathTime = Time.time;
			}
			if (currentWaypoint < path.vectorPath.Count)
			{
				dir = (path.vectorPath[currentWaypoint]-transform.position).normalized;
				if (path != null && Vector3.Distance (transform.position, path.vectorPath[currentWaypoint]) < nextWaypointDistance)
					currentWaypoint++;
			}
			else 
				dir = new Vector3();
			
			enemy.move (dir, 1.0f);
			
		}
		
		if(canAttack==true) enemy.primAttack();
		
		isVisible=canAttack=false;
	}
}
