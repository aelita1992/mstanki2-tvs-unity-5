﻿using UnityEngine;
using System.Collections;

public class Enemy4AI : AI {

	void Update () {
		checkVisibility();
		
		if (isVisible==true) checkAttack();
		
		if(isVisible==true && canAttack==false)
		{
			dir = (enemy.player.transform.position - transform.position).normalized;
			enemy.move (dir, 1.0f);
			
		}
		
		if (isVisible==false && lastPos != null)
		{
			if(path != null && pathTime + rnd < Time.time)
			{
				calcPath(lastPos);
				pathTime = Time.time;
				rnd = Random.Range (0.7f, 1.3f);
			}
			if (path != null && currentWaypoint < path.vectorPath.Count)
			{
				dir = (path.vectorPath[currentWaypoint]-transform.position).normalized;
				if (Vector3.Distance (transform.position, path.vectorPath[currentWaypoint]) < nextWaypointDistance)
					currentWaypoint++;
			}
			else 		
			if (path != null && currentWaypoint >= path.vectorPath.Count) { //osiagniety koniec sciezki
				lastPos = originalPos;
			}
			else 
				dir = new Vector3();
			
			enemy.move (dir, 1f);
			
		}
		if(canAttack==true) enemy.primAttack();
		
		isVisible=canAttack=false;
	}
}
