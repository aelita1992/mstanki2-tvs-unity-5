﻿using UnityEngine;
using System.Collections;

public class Shotgun : Weapon 
{

	public Shotgun() 
	{
		name = "Shotgun";
		fireRate = 0.8f;
		//speed = 25;
		maxAmmo = currentAmmo = 50;
		damage = 20;
	}

	public override void shoot()
	{
		if (Time.time > nextFire && Weapon.currentBullets > 9) {
			Instantiate (shotShotgun, shotSpawn.transform.position, shotSpawn.transform.rotation);
			nextFire = Time.time + fireRate;
			Weapon.currentBullets -= 10;
		}
	}
}
