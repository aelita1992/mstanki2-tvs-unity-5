﻿using UnityEngine;
using System.Collections;

public class SniperRifle : Weapon 
{

	public SniperRifle()
	{
		name = "Sniper Rifle";
		fireRate = 1.0f;
		speed = 1000;
		damage = 120;
		maxAmmo = currentAmmo = 10;
	}

	public override void shoot()
	{
		if (Time.time > nextFire && Weapon.currentBullets > 19) {
			Instantiate (shotBullet, shotSpawn.transform.position, shotSpawn.transform.rotation);
			nextFire = Time.time + this.fireRate;
			Weapon.currentBullets -= 19;
		}
	}
}
