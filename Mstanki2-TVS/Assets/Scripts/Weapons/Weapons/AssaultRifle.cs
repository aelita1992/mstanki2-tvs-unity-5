﻿using UnityEngine;
using System.Collections;

public class AssaultRifle : Weapon
{
	public AssaultRifle() 
	{
		name = "Assault Rifle";
		fireRate = 0.1f;
		speed = 500;
		accuracy = 15;
		damage = 25;
		//currentAmmo = maxAmmo = 200;
		Weapon.maxBullets = Weapon.currentBullets = 200;
		
	}

	public override void shoot()
	{
		if (Time.time > nextFire && Weapon.currentBullets > 0) {
			Instantiate (shotBullet, shotSpawn.transform.position, shotSpawn.transform.rotation);
			nextFire = Time.time + fireRate;
			Weapon.currentBullets--;
		}
	}
}
