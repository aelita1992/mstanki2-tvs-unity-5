﻿using UnityEngine;
using System.Collections;

public class RocketLauncher : Weapon 
{

	public RocketLauncher() 
	{
		name = "Rocket Launcher";
		fireRate = 1.3f;
		speed = 300;
		currentAmmo = maxAmmo = 5;
		damage = 150;
	}

	public override void shoot()
	{
		if (Time.time > nextFire && currentAmmo > 0) {
			Instantiate (shotRocket, shotSpawn.transform.position, shotSpawn.transform.rotation);
			nextFire = Time.time + fireRate;
			currentAmmo--;
		}
	}

	public void blow()
	{

	}
}
