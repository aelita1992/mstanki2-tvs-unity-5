﻿using UnityEngine;
using System.Collections;

public class CardRed : MonoBehaviour {
	
	PlayerController playerController;
	GameObject thePlayer;
	
	void Awake () {
		thePlayer = GameObject.FindGameObjectWithTag ("Player");
		playerController = thePlayer.GetComponent<PlayerController>();
	}
	
	
	void OnTriggerEnter2D(Collider2D other){
		if (other.tag == "Player") {
			playerController.card [1] = true;
			Debug.Log ("Red Card = true");
			Destroy (this.gameObject);
		}
	}
}
