﻿using UnityEngine;
using System.Collections;

public class DoorTrigger : MonoBehaviour {

	private PlayerController player;

	void Start()
	{
		//player = Game
	}
	void OnTriggerEnter2D(Collider2D other){		
		if (other.tag == "Player" && transform.parent.GetComponent<Door>().unlocked == true) {
			Debug.Log ("otworz drzwi");
			transform.parent.GetComponent<Door>().Open();
		}
	}
	void OnTriggerExit2D(Collider2D other){		
		if (other.tag == "Player" && transform.parent.GetComponent<Door>().unlocked == true) {
			Debug.Log("zamknij drzwi");
			transform.parent.GetComponent<Door>().Close();
		}
	}
}
