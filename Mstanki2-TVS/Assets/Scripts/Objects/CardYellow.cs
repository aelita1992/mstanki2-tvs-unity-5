﻿using UnityEngine;
using System.Collections;

public class CardYellow : MonoBehaviour {
	
	PlayerController playerController;
	GameObject thePlayer;
	
	void Awake () {
		thePlayer = GameObject.FindGameObjectWithTag ("Player");
		playerController = thePlayer.GetComponent<PlayerController>();
	}


	void OnTriggerEnter2D(Collider2D other){
				if (other.tag == "Player") {
						playerController.card [0] = true;
						Debug.Log ("Yellow Card = true");
						Destroy (this.gameObject);
				}
		}
}
