﻿using UnityEngine;
using System.Collections;

public class TurretBulletController : MonoBehaviour 
{
	private int dmg;
	public AudioClip shotSound;

	void OnEnable () 
	{
		AudioManager.Play (shotSound);
		dmg = 5;
	}

	void Update () 
	{
		transform.Translate (new Vector3(0, 5, 0));
	}
	
	void OnTriggerEnter2D(Collider2D other)
	{
		if(other.tag == "Player")
		{
			PlayerController.hit (dmg);
			TurretAmmoPool.Get ().ReleaseBullet(this.gameObject);
			return;
		}

		if (other.tag == "Sciana_niszczalna")
		{
			Destroy_wall wall = other.GetComponent<Destroy_wall> ();
			wall.hit(dmg);
			TurretAmmoPool.Get ().ReleaseBullet(this.gameObject);
			return;
		}

		if(other.tag != "Enemy" && other.tag != "Item" && other.tag != "Trigger")
			TurretAmmoPool.Get ().ReleaseBullet(this.gameObject);
		
	}
}
