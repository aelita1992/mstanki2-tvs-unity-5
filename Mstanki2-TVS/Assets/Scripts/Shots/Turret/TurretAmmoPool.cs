﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TurretAmmoPool : MonoBehaviour 
{
	public GameObject turretBulletPrefab;
	public GameObject turretRocketPrefab;

	private Stack <GameObject> bullets;
	private Stack <GameObject> rockets;

	private static TurretAmmoPool singleton;
	public static TurretAmmoPool Get()
	{
		return singleton;
	}

	private TurretAmmoPool(){		}

	void Awake()
	{
		singleton = this;
		bullets = new Stack <GameObject>();
		rockets = new Stack <GameObject>();

		for(int i = 0; i < 10; i++)
		{
			GameObject bullet = (GameObject)Instantiate (turretBulletPrefab);
			bullets.Push (bullet);
			bullet.SetActive (false);
		}

		for(int i = 0; i < 10; i++)
		{
			GameObject rocket = (GameObject)Instantiate (turretRocketPrefab);
			rockets.Push (rocket);
			rocket.SetActive (false);
		}
	}

	public GameObject GetBullet()
	{
		if(bullets.Count > 0)
			return bullets.Pop ();

		return (GameObject)Instantiate (turretBulletPrefab);
	}

	public void ReleaseBullet(GameObject releasedBullet)
	{
		bullets.Push (releasedBullet);
		releasedBullet.SetActive (false);
	}

	public GameObject GetRocket()
	{
		if(rockets.Count > 0)
			return rockets.Pop ();
		
		return (GameObject)Instantiate (turretRocketPrefab);
	}
	
	public void ReleaseRocket(GameObject releasedRocket)
	{
		rockets.Push (releasedRocket);
		releasedRocket.SetActive (false);
	}
}
