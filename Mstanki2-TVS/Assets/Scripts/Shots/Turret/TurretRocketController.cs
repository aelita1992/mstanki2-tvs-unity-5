﻿using UnityEngine;
using System.Collections;

public class TurretRocketController : MonoBehaviour
{
	public ParticleSystem particle;
	private int dmg;
	// Use this for initialization
	void Start ()
	{
		dmg = 10;
	}
	
	// Update is called once per frame
	void Update () 
	{
		transform.Translate (new Vector3(0, 10, 0));
	}
	
	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.tag != "Enemy" && other.tag != "Bullet" && other.tag != "Item" && other.tag != "Trigger")
		{
			blow ();
		}
	}
	
	public void blow()
	{
		RocketBlow coll;
		particle.Play ();
		Instantiate (particle, transform.position, transform.rotation);
		coll = GetComponentInChildren<RocketBlow>();
		for (int i=0; i<coll.enemies.Count; i++) coll.enemies[i].hit(dmg);
		if (coll.player == true) PlayerController.hit (dmg/2);

		TurretAmmoPool.Get ().ReleaseRocket(this.gameObject);
	}
}
