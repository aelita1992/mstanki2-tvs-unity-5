﻿using UnityEngine;
using System.Collections;

public class ShotgunMover : MonoBehaviour {

	//public float speed;
	//private bool isMoving;
	//private Vector3 sp, dir;
	//private float speed;
	private int life;
	private int dmg;
	private Vector3 sp, dir;
	private float knockbackTime;
	// Use this for initialization
	void Start () {
		//isMoving = false;
		life = 0;
		knockbackTime = 0.05f;

		sp = Camera.main.WorldToScreenPoint(gameObject.transform.position);
		dir = (InputMapper.pointer - sp).normalized;;
		transform.Rotate (0, 0, 180);
		//transform.position += 50 * dir;

		dmg = WeaponManager.Get ().getCurrentWeapon ().getDMG();
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (life == 5)
			Destroy (this.gameObject);
		else
			life++;
	}


	void OnTriggerEnter2D(Collider2D other)
	{
		if(other.tag == "Enemy")
		{
			Enemy enemyLife = other.GetComponent<Enemy>();
			enemyLife.hit (dmg);
			Vector3 knockbackVector = enemyLife.transform.position - this.transform.position;
			enemyLife.knockback (knockbackTime, knockbackVector);
			//GameObject.Destroy (this.gameObject);
			Debug.Log ("HIT");
			return;
		}
		if (other.tag == "Sciana_niszczalna")
		{
			Destroy_wall wall = other.GetComponent<Destroy_wall> ();
			wall.hit(dmg);
			//GameObject.Destroy(this.gameObject);
			return;
		}
		if (other.tag != "Player" && other.tag != "Item") {
		}
		
	}
}