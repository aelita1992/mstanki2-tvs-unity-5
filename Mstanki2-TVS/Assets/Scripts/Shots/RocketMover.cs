﻿using UnityEngine;
using System.Collections;

public class RocketMover : MonoBehaviour {

	//public float speed;
	private bool isMoving;
	private Vector3 sp, dir;
	private float speed;
	public ParticleSystem particle;
	private int dmg;
	// Use this for initialization
	void Start () {
		isMoving = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (!isMoving) {
			sp = Camera.main.WorldToScreenPoint(transform.position);
			dir = (InputMapper.pointer - sp).normalized;
			speed = WeaponManager.Get ().getCurrentWeapon().getSpeed();
			dmg = WeaponManager.Get ().getCurrentWeapon ().getDMG();


			isMoving = true;	
		}

		if (isMoving) 
		{
			transform.position = transform.position + dir * Time.deltaTime * speed;
			transform.position = new Vector3(transform.position.x, transform.position.y, 0);
		}
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.tag != "Player" && other.tag != "Bullet" && other.tag != "Item" && other.tag != "Trigger")
		{
			blow ();
			//Debug.Log (other.tag);
		}
	}
	
	public void blow()
	{
		RocketBlow coll;
		particle.Play ();
		GameObject.Destroy(this.gameObject);
		Instantiate (particle, transform.position, transform.rotation);
		coll = GetComponentInChildren<RocketBlow>();
		for (int i=0; i<coll.enemies.Count; i++) coll.enemies[i].hit(dmg);
		if (coll.player == true) PlayerController.hit (dmg/2);
	}
}
