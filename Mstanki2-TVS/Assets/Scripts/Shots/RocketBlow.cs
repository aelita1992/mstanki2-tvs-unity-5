﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RocketBlow : MonoBehaviour {

	//public RocketMover rocket;
	//public ArrayList enemies = new ArrayList();
	public List<Enemy> enemies = new List<Enemy>();
	public bool player = true;

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.tag == "Enemy")
		{
			Enemy enemy = other.GetComponent<Enemy>();
			enemies.Add (enemy);
		}
		if (other.tag == "Player") player = true;
	}
	

	void OnTriggerExit2D(Collider2D other)
	{
		if (other.tag == "Enemy")
		{
			Enemy enemy = other.GetComponent<Enemy>();
			enemies.Remove (enemy);
		}
		if (other.tag == "Player") player = false;
	}
}