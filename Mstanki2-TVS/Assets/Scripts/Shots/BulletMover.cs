﻿using UnityEngine;
using System.Collections;

public class BulletMover : MonoBehaviour {

	//public float speed;
	private bool isMoving;
	private Vector3 sp, dir;
	private float speed;
	private int dmg;
	private float turnSpeed;
	public AudioClip shotSound;
	//private AudioSource audioSource;

	// Use this for initialization
	void Start () {
		isMoving = false;
		turnSpeed = 10.0f;
		//audioSource = gameObject.GetComponent<AudioSource> ();
		AudioManager.Play (shotSound);
		
	}
	
	// Update is called once per frame
	void Update () {
		// Pozycja przed zwiększeniem używana do raycastingu
		Vector3 cast_vector;

		// Klasa raycastowa
		RaycastHit2D castro;
		RaycastHit2D castro1;
		RaycastHit2D castro2;

		if (!isMoving) {
			sp = Camera.main.WorldToScreenPoint(transform.position);
			int s = WeaponManager.Get ().getCurrentWeapon ().getAccuracy();
			Vector3 spread = new Vector3(Random.Range (-s,s), Random.Range (-s,s));
			dir = (InputMapper.pointer - sp + spread).normalized;
			speed = WeaponManager.Get ().getCurrentWeapon().getSpeed();
			dmg = WeaponManager.Get ().getCurrentWeapon ().getDMG();

			float targetAngle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
			//gameObject.transform.rotation = Quaternion.Slerp(gameObject.transform.rotation, Quaternion.Euler(0, 0, targetAngle), turnSpeed * Time.deltaTime);

			isMoving = true;	
		}

		if (isMoving) 
		{
			//Zapisuje pozycję przed przesunięciem pocisku *raycasting*
			cast_vector = transform.position;

			transform.position = transform.position + dir * Time.deltaTime * speed;
			transform.position = new Vector3 (transform.position.x, transform.position.y, 0);


			//Przypisanie raycastowi linii pomiędzy 2 punktami(przesynięciem i poprzednim lokum)
			castro = Physics2D.Linecast(transform.position,sp);

			/*
			if(castro.collider!=null){
					if(castro.collider.gameObject.tag=="Wall"){
					Debug.Log(castro.collider.gameObject.tag);
					GameObject.Destroy (this.gameObject);

				}
			}
			*/

				



		}
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if(other.tag == "Enemy")
		{
			Enemy enemyLife = other.GetComponent<Enemy>();
			enemyLife.hit (dmg);
			GameObject.Destroy (this.gameObject);
			return;
		}
		if (other.tag == "Sciana_niszczalna")
		{
			Destroy_wall wall = other.GetComponent<Destroy_wall> ();
			wall.hit(dmg);
			GameObject.Destroy(this.gameObject);
			return;
		}

		if(other.tag != "Player" && other.tag != "Item" && other.tag != "Trigger")
			GameObject.Destroy(this.gameObject);
		
	}
}
