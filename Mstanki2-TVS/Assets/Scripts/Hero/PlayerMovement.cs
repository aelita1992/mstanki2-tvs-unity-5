﻿using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour
{
	public float speed;
	public Camera camera;
	private Vector3 sp, dir;
	private float turnSpeed = 2f;
	private bool isMoving;
	
	Animator movementAnimator;
	Vector3 movementSpeed;

	
	void Start()
	{
		movementAnimator = this.gameObject.GetComponent<Animator>();
		isMoving = false;
	}
	
	/*void Update ()
	{
		float moveHorizontal = Input.GetAxis ("Horizontal");
		float moveVertical = Input.GetAxis ("Vertical");
		
		if (moveHorizontal != 0 || moveVertical != 0) 
		{
			movementAnimator.SetBool ("isMoving", true);
			Vector3 dir = new Vector3(moveHorizontal, moveVertical, 0);
			dir.Normalize();
			transform.position = transform.position + dir*speed*Time.deltaTime;

		} else 
		{
			movementAnimator.SetBool ("isMoving", false);
		}
	} */
	
	public void moveUp()
	{
		/*movementAnimator.SetBool ("isMoving", true);
		Vector3 dir = new Vector3(0, 1, 0);
		//dir.Normalize();
		transform.position = transform.position + dir*speed*Time.deltaTime;*/
		RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.up, 8.5f);
		
		if (hit.collider != null) 
		{
			if(hit.collider.gameObject.tag != "Wall")	movementSpeed.y = 1;
		}
		else
		{
			movementSpeed.y = 1;
		}
	}
	
	public void moveDown()
	{
		/*movementAnimator.SetBool ("isMoving", true);
		Vector3 dir = new Vector3(0, -1, 0);
		//dir.Normalize();
		transform.position = transform.position + dir*speed*Time.deltaTime;*/
		RaycastHit2D hit = Physics2D.Raycast(transform.position, -Vector2.up, 8.5f);
		
		if (hit.collider != null) 
		{
			if(hit.collider.gameObject.tag != "Wall")	movementSpeed.y = -1;
		}
		else
		{
			movementSpeed.y = -1;
		}
	}
	public void moveLeft()
	{
		/*movementAnimator.SetBool ("isMoving", true);
		Vector3 dir = new Vector3(-1, 0, 0);
		//dir.Normalize();
		transform.position = transform.position + dir*speed*Time.deltaTime;*/
		RaycastHit2D hit = Physics2D.Raycast(transform.position, -Vector2.right, 8.5f);
		
		if (hit.collider != null) 
		{
			if(hit.collider.gameObject.tag != "Wall")	movementSpeed.x = -1;
		}
		else
		{
			movementSpeed.x = -1;
		}
	}
	public void moveRight()
	{
		/*movementAnimator.SetBool ("isMoving", true);
		Vector3 dir = new Vector3(1, 0, 0);
		//dir.Normalize();
		transform.position = transform.position + dir*speed*Time.deltaTime;*/
		RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.right, 8.5f);
		
		if (hit.collider != null) 
		{
			if(hit.collider.gameObject.tag != "Wall")	movementSpeed.x = 1;
		}
		else
		{
			movementSpeed.x = 1;
		}
	}
	
	void Update()
	{
		sp = Camera.main.WorldToScreenPoint(gameObject.transform.position);
		dir = (Input.mousePosition - sp).normalized;
		Vector3 currPosition = gameObject.transform.position;
		
		currPosition += movementSpeed;
		gameObject.transform.position = currPosition;
		
		movementSpeed = Vector3.zero;
		float targetAngle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg - 90;
		gameObject.transform.rotation = Quaternion.Slerp(gameObject.transform.rotation, Quaternion.Euler(0, 0, targetAngle), turnSpeed * Time.deltaTime);
		//camera.transform.position = [gameObject.transform.position.x, gameObject.transform.position.y, -3] ;
		Vector3 a = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		//camera.transform.position = new Vector3 ((a.x + this.transform.position.x)/2, (a.y + this.transform.position.y)/2, -3.0f);

		camera.transform.position = new Vector3 
			( Mathf.Clamp((a.x + this.transform.position.x)/2 ,gameObject.transform.position.x -40, this.transform.position.x +40), 
			  Mathf.Clamp((a.y + this.transform.position.y)/2 ,gameObject.transform.position.y -40, this.transform.position.y +40),
			 -3.0f
			 );
	                                                 
		/*if (!isMoving) 
		{
			camera.transform.position = new Vector3 (gameObject.transform.position.x , gameObject.transform.position.y, -3.0f);
			isMoving = true;	
		}
		
		if (isMoving) 
		{
			camera.transform.position = new Vector3 ((a.x + this.transform.position.x)/2, (a.y + this.transform.position.y)/2, -3.0f);
		}
		*/
	}
	
	
	
	
}