﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

	TextMesh text;
	public GameObject shotBullet, shotRocket, shotGrenade, shotShotgun;
	public GameObject shotSpawn;
	public static int HP, shield, energy;
	public static bool isDead;
	public bool[] card = new bool[3];
	public AudioClip walkSound;
	private float walkSoundLast = 0.0f;

	private float shieldRechargeTimer;
	private float energyRechargeTimer;
	private static GameObject player;
	public LineRenderer laser;
	private RaycastHit2D hitCast;
	private LayerMask layerMask;
	//private AudioSource stepSound;

	//PlayerMovement.cs
	public float speed;
	public Camera camera;
	public Vector3 sp, dir, temp;
	private float turnSpeed = 10f;
	private bool isMoving;
	private bool isRotate;
	
	Animator movementAnimator;
	Vector3 movementSpeed;
	

	void Start () {
		text = gameObject.GetComponentInChildren<TextMesh>();
		WeaponManager.Get ().setTextMesh (text);
		WeaponManager.Get ().changeWeapon (1);
		WeaponManager.Get ().getCurrentWeapon ().setShot (shotBullet, shotRocket, shotGrenade, shotShotgun);
		WeaponManager.Get ().getCurrentWeapon ().setShotSpawn (shotSpawn);

		isDead = false;
		HP=100;
		shield=100;
		energy=100;

		shieldRechargeTimer = 0;

		player = GameObject.FindGameObjectWithTag("Player");


		//PlayerMovement.cs
		movementAnimator = this.gameObject.GetComponent<Animator>();
		isMoving = false;

		//stepSound = FindObjectOfType<AudioSource> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (!isDead)
		{
			shieldAutoregen();
			energyAutoregen();
			drawLaser();
		}

		if(movementSpeed == Vector3.zero)
		{
			movementAnimator.SetBool ("isMoving", false);
			//stepSound.Stop();
		}
		else
		{
			movementAnimator.SetBool ("isMoving", true);
			//if(stepSound.isPlaying == false) stepSound.Play ();
			if(Time.time > walkSoundLast + walkSound.length)
			{
				AudioManager.Play (walkSound);
				walkSoundLast = Time.time;
			}

		}

		//PlayerMovement.cs
		sp = Camera.main.WorldToScreenPoint(gameObject.transform.position);
		dir = (InputMapper.pointer - sp).normalized;
		/*
			if(movementSpeed == Vector3.zero)
			{
				movementAnimator.SetBool ("isMoving", false);
			}
			else
		    {
				movementAnimator.SetBool ("isMoving", true);
			}
	    */
		Vector3 currPosition = gameObject.transform.position;
		
		currPosition += movementSpeed;
		gameObject.transform.position = currPosition;
		
		movementSpeed = Vector3.zero;



		if(!isDead) 
		{
			float targetAngle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg + 90;
			gameObject.transform.rotation = Quaternion.Slerp(gameObject.transform.rotation, Quaternion.Euler(0, 0, targetAngle), turnSpeed * Time.deltaTime);
		}


		//camera.transform.position = [gameObject.transform.position.x, gameObject.transform.position.y, -3] ;
		Vector3 a = Camera.main.ScreenToWorldPoint(InputMapper.pointer);
		//camera.transform.position = new Vector3 ((a.x + this.transform.position.x)/2, (a.y + this.transform.position.y)/2, -3.0f);
		
		camera.transform.position = new Vector3 
			( Mathf.Clamp((a.x + this.transform.position.x)/2 ,gameObject.transform.position.x -40, this.transform.position.x +40), 
			 Mathf.Clamp((a.y + this.transform.position.y)/2 ,gameObject.transform.position.y -40, this.transform.position.y +40),
			 -3.0f
			 );
	}

	void FixedUpdate ()
	{
		sp = Camera.main.WorldToScreenPoint(gameObject.transform.position);
		dir = (InputMapper.pointer - sp).normalized;

		/*
		if(dir==temp)
		{
			movementAnimator.SetBool ("isMoving", false);
		}
		else
		{
			movementAnimator.SetBool ("isMoving", true);
		}
		//*/
		//Input.mousePosition = temp;
		//dir = temp;
	}

	//PlayerMovement.cs
	public void moveUp()
	{
		//movementAnimator.SetBool ("isMoving", true);
		/*
		Vector3 dir = new Vector3(0, 1, 0);
		//dir.Normalize();
		transform.position = transform.position + dir*speed*Time.deltaTime;*/
		RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.up, 8.5f);
		
		if (hit.collider != null) 
		{
			if(hit.collider.gameObject.tag != "Wall")	movementSpeed.y = 1;
		}
		else
		{
			movementSpeed.y = 1;
		}
	}
	
	public void moveDown()
	{
		//movementAnimator.SetBool ("isMoving", true);
		/*Vector3 dir = new Vector3(0, -1, 0);
		//dir.Normalize();
		transform.position = transform.position + dir*speed*Time.deltaTime;*/
		RaycastHit2D hit = Physics2D.Raycast(transform.position, -Vector2.up, 8.5f);
		
		if (hit.collider != null) 
		{
			if(hit.collider.gameObject.tag != "Wall")	movementSpeed.y = -1;
		}
		else
		{
			movementSpeed.y = -1;
		}
	}
	public void moveLeft()
	{
		//movementAnimator.SetBool ("isMoving", true);
		/*Vector3 dir = new Vector3(-1, 0, 0);
		//dir.Normalize();
		transform.position = transform.position + dir*speed*Time.deltaTime;*/
		RaycastHit2D hit = Physics2D.Raycast(transform.position, -Vector2.right, 8.5f);
		
		if (hit.collider != null) 
		{
			if(hit.collider.gameObject.tag != "Wall")	movementSpeed.x = -1;
		}
		else
		{
			movementSpeed.x = -1;
		}
	}
	public void moveRight()
	{
		//movementAnimator.SetBool ("isMoving", true);
		/*Vector3 dir = new Vector3(1, 0, 0);
		//dir.Normalize();
		transform.position = transform.position + dir*speed*Time.deltaTime;*/
		RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.right, 8.5f);
		
		if (hit.collider != null) 
		{
			if(hit.collider.gameObject.tag != "Wall")	movementSpeed.x = 1;
		}
		else
		{
			movementSpeed.x = 1;
		}
	}

	public static void shieldRecharge()
	{
		if (energy > 2 && shield < 100)
		{
			energy = energy - 2;
			shield++;
		}
	}

	private void shieldAutoregen()
	{
		if (shieldRechargeTimer + 1 < Time.time) 
		{
			shield++;
			shieldRechargeTimer = Time.time;
		}

		if (shield > 100) shield = 100;
	}

	private void energyAutoregen()
	{
		if (energyRechargeTimer + 1 < Time.time) 
		{
			energy++;
			energyRechargeTimer = Time.time;
		}
		if (energy > 100) energy = 100;
	}

	public static void pickItem(int item, int q)
	{
		switch(item) {
		case 0:
			HP += q;
			if (HP>100) HP=100;
			break;

		case 1:
			shield += q;
			if (shield>100) shield=100;
			break;

		case 2:
			WeaponManager.Get ().pickAmmo(q);
			break;

		case 3:
			WeaponManager.Get ().pickRocket(q);
			break;

		case 4:
			WeaponManager.Get ().pickGrenade(q);
			break;
		}

	}

	public static void hit (int dmg) {
		int rem=dmg;
		if(shield >= dmg) {
			shield -=dmg;
			return;
		}
		if(shield < dmg) {
			//rem = dmg - shield;
			dmg -=shield;
			shield = 0;
		}
		if(HP >= dmg) {
			HP -= dmg;
			return;
		}
		if (HP < dmg) {
			HP = 0;
			die();
			return;
		}
	}

	private void drawLaser()
	{
		layerMask = 1 << 8 | 1 << 2;
		layerMask =  ~layerMask;
		hitCast = Physics2D.Raycast (transform.position , dir, Mathf.Infinity, layerMask);
		laser.SetPosition (0, transform.position);
		laser.SetPosition (1, hitCast.point);
	}

	public static Vector3 getPosition()
	{
		return player.transform.position;
	}

	private static void die()
	{
		isDead = true;
	}
}
