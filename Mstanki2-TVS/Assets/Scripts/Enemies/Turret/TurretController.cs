﻿using UnityEngine;
using System.Collections;

public class TurretController : MonoBehaviour
{


	public GameObject[] cores;
	public GameObject[] supporters;

	public void Awake()
	{
		foreach(GameObject core in cores)
		{
			core.GetComponent<Animator>().SetBool("activationTriggered", true);
		}

		foreach(GameObject supporter in supporters)
		{
			supporter.GetComponent<Animator>().SetBool("activationTriggered", true);
		}
	}

	public void OpenFire(Vector2 targetPosition)
	{
		foreach(GameObject core in cores)
		{
			core.GetComponent<TurretCore>().FireAtPosition(targetPosition);
		}
		
		foreach(GameObject supporter in supporters)
		{
			supporter.GetComponent<TurretSupporter>().FireAtPosition(targetPosition);
		}
	}
}
