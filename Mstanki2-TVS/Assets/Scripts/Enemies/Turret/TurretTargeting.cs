﻿using UnityEngine;
using System.Collections;

public class TurretTargeting : MonoBehaviour
{
	delegate void TurretDelegate();
	static event TurretDelegate TurretAction;

	public TurretController turretController;
	public LayerMask targetsMask;
	public LineRenderer laserRenderer;

	public Vector2 angleBoundaries;

	float currentAngle;
	float rotationSpeed;

	void Start () 
	{
		rotationSpeed = 0.1f;
		TurretAction += Targeting;
	}

	void FixedUpdate ()
	{
		if(TurretAction != null)
			TurretAction();
	}

	void Targeting()
	{
		RotateTurret();
		
		Vector3 direction = transform.TransformDirection(Vector3.down);
		
		Vector2 position2d = new Vector3(transform.position.x, transform.position.y);
		Vector2 direction2d = new Vector2(direction.x, direction.y);
		
		RaycastHit2D hit;
		hit = Physics2D.Raycast(position2d, direction2d, 3000f, targetsMask);
		
		if(hit)
		{
			laserRenderer.SetPosition(0, Vector3.zero);
			float rayLength = hit.point.y - position2d.y;
			laserRenderer.SetPosition(1, new Vector2(0, rayLength));

			if(hit.collider.gameObject.layer == LayerMask.NameToLayer("Player"))
			{
				TurretAction -= Targeting;

				Vector3 targetCenter = hit.collider.transform.position;
				Vector2 targetCenter2D = new Vector2(targetCenter.x, targetCenter.y);
				turretController.OpenFire(targetCenter2D);
			}
		}
	}

	void RotateTurret()
	{
		transform.Rotate (new Vector3(0, 0, rotationSpeed));
		currentAngle += rotationSpeed;

		if(currentAngle > angleBoundaries.y)
			rotationSpeed = -0.1f;

		if(currentAngle < angleBoundaries.x)
			rotationSpeed = 0.1f;
	}
}
