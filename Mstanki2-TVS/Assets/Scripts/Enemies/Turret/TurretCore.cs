﻿using UnityEngine;
using System.Collections;

public class TurretCore : MonoBehaviour 
{
	delegate void TurretDelegate();
	static event TurretDelegate TurretAction;
	
	Vector2 firingDirection;
	Vector2 targetPosition;
	
	float firingTimer;
	float nextShotTime;
	
	public void FireAtPosition(Vector2 targetPos)
	{
		targetPosition = targetPos;
		Vector2 corePos2D = new Vector2(transform.position.x, transform.position.y);
		Vector2 direction = targetPos - corePos2D;
		
		firingDirection = direction.normalized;
		firingTimer = 10;
		nextShotTime = 9.5f;
		TurretAction += Firing;
	}
	
	void Firing()
	{
		firingTimer -= Time.deltaTime;
		
		if(firingTimer < nextShotTime)
		{
			Vector2 corePos2D = new Vector2(transform.position.x, transform.position.y);
			
			nextShotTime -= 0.5f;
			GameObject rocket = TurretAmmoPool.Get().GetRocket();
			rocket.transform.position = this.transform.position;
			
			float angle = Vector3.Angle(Vector3.up, firingDirection);
			if(targetPosition.x > transform.position.x)
				angle = 360 - angle;
			
			float spreadParameter = Random.Range(-2f, 2f);
			
			rocket.transform.eulerAngles = new Vector3(0, 0, angle + spreadParameter);
			rocket.SetActive(true);
		}
		
		if(firingTimer < 0)
			TurretAction -= Firing;
	}
	
	void FixedUpdate()
	{
		if(TurretAction != null)
			TurretAction();
	}
}
