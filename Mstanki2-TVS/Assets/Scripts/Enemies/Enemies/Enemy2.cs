﻿using UnityEngine;
using System.Collections;

public class Enemy2 : Enemy {

	public GameObject shotSpawn;
	public GameObject shotBullet;
	//private Vector3 originalPos;
	public ChangeSprite tl, tm, tr,
						 ml, mm, mr,
						 bl, bm, br;

	void Start () {
		lastPrimAttack=0;
		lastSecAttack=0;

		animator = null;
	}


	public override void primAttack ()
	{
		if (lastPrimAttack + primAttackRate < Time.time) {
			Instantiate (shotBullet, shotSpawn.transform.position, shotSpawn.transform.rotation);
			lastPrimAttack = Time.time;


		}
		Vector3 dir = (PlayerController.getPosition() - this.transform.position).normalized;
		float targetAngle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg - 90; 
		gameObject.transform.rotation = Quaternion.Slerp(gameObject.transform.rotation, Quaternion.Euler(0, 0, targetAngle), turnSpeed * Time.deltaTime);

	}

	public override void die()
	{
		GameObject.Destroy(this.gameObject);
		//foreach (ChangeSprite sp in sprites) sp.change ();
		//SendMessage ("change");
		tl.change (); tm.change (); tr.change ();
		ml.change (); mm.change (); mr.change ();
		bl.change (); bm.change (); br.change ();

	}
}
