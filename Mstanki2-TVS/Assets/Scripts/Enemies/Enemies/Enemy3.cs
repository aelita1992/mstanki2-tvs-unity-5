﻿using UnityEngine;
using System.Collections;

public class Enemy3 : Enemy {

	public AnimationClip blowAnimation;
	public float blowRadius;
	private float attackTime = 0;
	private bool isBlowed = false;
	private bool isBlowing = false;
	void Start()
	{
		//animator = null;
	}

	void Update()
	{
		if(isBlowing) blow ();
	}

	public override void primAttack ()
	{
		if (attackTime == 0) {
			attackTime = Time.time;
			animator.Play ("Attack");
			speed = 0.0f;
			Destroy (this.gameObject, blowAnimation.length - 0.05f);
			isBlowing = true;
		}
	}

	public void blow()
	{
		if (isBlowed==false && attackTime + blowAnimation.length - 0.1f <= Time.time) {
			float dist = Vector3.Distance (this.transform.position, PlayerController.getPosition ());
			Debug.Log (dist);
			if(dist<blowRadius){
				PlayerController.hit (primAttackDMG);
				Debug.Log ("HIT: " + primAttackDMG);

			}
		isBlowed = true;
		}

	}

	
}
