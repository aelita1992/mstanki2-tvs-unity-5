﻿using UnityEngine;
using System.Collections;

public class Enemy4 : Enemy {

	public GameObject shotSpawn;
	public GameObject shotBullet;
	public GameObject turret;
	private float orgSpeed;
	private Vector3 pos, prevPos;
	//private float turnSpeed;

	void Start () {
		lastPrimAttack=0;
		lastSecAttack=0;
		animator.Play ("Idle");
		orgSpeed = speed;
		pos = prevPos = transform.position;
		//turnSpeed = 5f;
	}

	void Update() {
		pos = transform.position;
		if (prevPos.x == pos.x && prevPos.y == pos.y)
			animator.Play ("Idle");
		prevPos = pos;
	}
	public override void primAttack ()
	{
		if (lastPrimAttack + primAttackRate < Time.time) {
			Instantiate (shotBullet, shotSpawn.transform.position, shotSpawn.transform.rotation);
			lastPrimAttack = Time.time;
			//speed=0.0f;
			animator.Play ("Attack");
			turret.GetComponent<Animator>().Play ("Attack");
		}

		Vector3 dir = (PlayerController.getPosition() - this.transform.position).normalized;
		float targetAngle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg - 90;
		gameObject.transform.rotation = Quaternion.Slerp(gameObject.transform.rotation, Quaternion.Euler(0, 0, targetAngle), turnSpeed * Time.deltaTime);
	}
}
