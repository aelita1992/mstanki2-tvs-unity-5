﻿using UnityEngine;
using System.Collections;

public class StateMachine : MonoBehaviour {

	enum State {MENU, IN_GAME};

	State ActualState;

	public GameObject menu;


	void Start () {


		ActualState = State.MENU;
		menu.SetActive (true);
	}

	State ChangeState()
	{
				if (ActualState == State.MENU) {
						ActualState = State.IN_GAME;
						menu.SetActive(false);
				}
				else {
						ActualState = State.MENU;
						menu.SetActive(true);
				}
		return ActualState;
	}

	void Update () {

		if(Input.GetKeyDown(KeyCode.Escape))
		{
			ChangeState ();
			Debug.Log (ActualState);
		}


	}

	
}
