﻿using UnityEngine;
using System.Collections;

public class PlayerControler: MonoBehaviour
{
	void FixedUpdate ()
	{ 
		float moveHorizontal = Input.GetAxis ("Horizontal");
		float moveVertical = Input.GetAxis ("Vertical");
		Vector3 Movement = new Vector3 (moveHorizontal, moveVertical, 0.0f);
		transform.position += Movement * 70 * Time.deltaTime;
	}
}
