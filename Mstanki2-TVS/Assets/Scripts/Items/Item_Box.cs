﻿using UnityEngine;
using System.Collections;

public class Item_Box : MonoBehaviour {
	
	// Ile sztuk w paczce
	public int Quantity;
	
	// Zawartość paczki
	public int Type;
	//0 - HP 1 - shield 2 - ammo 3 - rocket 4 -grenade
	
	
	
	
	void OnTriggerEnter2D(Collider2D other) {
		if (other.tag == "Player") {
			Debug.Log("Item has been picked up.");
			PlayerController.pickItem(Type, Quantity);
			Destroy (this.gameObject);
			
		}
	}
	
	
}