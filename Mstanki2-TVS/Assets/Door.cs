﻿using UnityEngine;
using System.Collections;

public class Door : MonoBehaviour {

	public int distance = 100;
	//public float X = 1f;
	//public float Y = 0f;
	public float speed = 15f;
	public Vector3 vector;
	private Vector3 position;

	private int i;
	private int b;
	
	bool isOpen = false;
	bool isOpening = false;
	bool isClosing = false;

	public bool unlocked;

	// Use this for initialization
	void Start () {
		vector.z = 0;
		vector.Normalize ();
		position = transform.position;
		i = b = 0;
	}
	
	// Update is called once per frame
	void Update () {
		if (isOpening) {
			transform.position = transform.position + vector * speed * Time.deltaTime;
			i++;
			b=i;
			if (i >= distance) {
				isOpening = false;
				isOpen = true;
				i = 0; 
				//b = distance;
			}
			
		}
		if (isClosing) {
			transform.position = transform.position - vector * speed * Time.deltaTime;
			b--;
			i=b;
			if (b<=0) {
				isClosing = false;
				isOpen = false;
				i = 0;
				b = 0;
			}
		}
	
	}

	public void Open()
	{
		//if (isClosing == true)
			isClosing = false;
		isOpening = true;
	}

	public void Close()
	{
		//if (isOpening == true)
			isOpening = false;
		isClosing = true;
		//b = i;
	}
}
